package main

import (
	"github.com/visualfc/go-ui/ui"
	"os"
)

func main() {
	os.Exit(ui.Main(mainFunc))
}

func mainFunc() {
	w := NewMainUI()
	if w == nil {
		ui.Exit(1)
	}

	w.Init()
	w.Show()
	ui.Run()
}
