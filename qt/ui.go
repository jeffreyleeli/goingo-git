package main

import (
	"github.com/visualfc/go-ui/ui"
)

type mainUI struct {
	ui.Widget

	buttonInstall *ui.Button
	buttonCompile *ui.Button
	buttonConfig  *ui.Button
	buttonExit    *ui.Button

	configUI *configUI
}

func NewMainUI() *mainUI {
	return new(mainUI).Init()
}

func (p *mainUI) Init() *mainUI {
	if p.Widget.Init() == nil {
		return nil
	}

	p.buttonInstall = ui.NewButtonWithText("Install")
	p.buttonInstall.OnClicked(func() {
	})

	p.buttonCompile = ui.NewButtonWithText("Compile")
	p.buttonCompile.OnClicked(func() {
	})

	p.buttonConfig = ui.NewButtonWithText("Config")
	p.buttonConfig.OnClicked(func() {
		p.configUI = NewConfigUI()
		p.configUI.Show()
	})

	p.buttonExit = ui.NewButtonWithText("Exit")
	p.buttonExit.OnClicked(func() {
		p.Close()
	})

	layoutMain := ui.NewVBoxLayout()
	layoutMain.AddWidget(p.buttonInstall)
	layoutMain.AddWidget(p.buttonCompile)
	layoutMain.AddWidget(p.buttonConfig)
	layoutMain.AddWidget(p.buttonExit)

	p.SetWindowTitle("GoINGo")
	p.SetSizev(500, 600)
	p.SetLayout(layoutMain)

	return p
}
